import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FabMenuComponent } from   '../../components/fab-menu/fab-menu';
import { NewOrderComponent } from '../../components/new-order/new-order';
import { ServiceComponent } from '../components/service/service';
import { LoaderComponent } from '../components/loader/loader';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the ServicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-service',
  templateUrl: 'service.html',
})
export class ServicePage {
	serviceStatus= 'new';
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicePage');
  }
  sendOrder(){
  	this.serviceStatus="loading";
  	setTimeout( () => {
     this.serviceStatus="service";
}, 3000);
  }
}
