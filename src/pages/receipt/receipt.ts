import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FabMenuComponent } from '../../components/fab-menu/fab-menu';
/**
 * Generated class for the ReceiptPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-receipt',
  templateUrl: 'receipt.html',
})
export class ReceiptPage {
	order=
	{
	    "id": "-KxsN2PK_ZGJPZnN4VEJ",
	    "serviceType": {
	      "type": "tow",
	      "icon": "mdi mdi-car-hatchback",
	      "txt": "Servicio de grúa"
	    },
	    "date": "2014-06-25T12:34:56.000Z",
	    "completed": true,
	    "totalTime": "47",
	    "location": {
	      "lat": "19.3961817",
	      "lon": "-99.2731114",
	      "txt": "Fuente de Paseo 3, Lomas de las Palmas, 52788 Naucalpan de Juárez, Méx."
	    },
	    "status": [
	      {
	        "txt": "Buscando proveedores cercanos...",
	        "timestamp": "2014-06-25T12:34:56.000Z",
	        "checked": true
	      },
	      {
	        "txt": "Proveedores encontrados. Asignando...",
	        "timestamp": "2014-06-25T12:44:12.000Z",
	        "checked": true
	      },
	      {
	        "txt": "Proveedor en camino...",
	        "timestamp": "2014-06-25T12:55:35.000Z",
	        "checked": true
	      },
	      {
	        "txt": "Servicio en curso",
	        "timestamp": "2014-06-25T13:04:42.000Z",
	        "checked": true
	      },
	      {
	        "txt": "Servicio completo",
	        "timestamp": "2014-06-25T13:27:02.000Z",
	        "checked": true
	      }
	    ],
	    "userData": {
	      "uuid": "0xv19amNS7Ys18G76Ir86jHYI1K2",
	      "name": "Nicolas Z.",
	      "avatar": "http://www.qygjxz.com/data/out/190/6179593-profile-pics.jpg"
	    },
	    "vehicle": {
	      "id": "-Kq3tPRd_erDTlUOnI2d",
	      "brand": "VW",
	      "model": "Jetta",
	      "plate": "MKS 1287"
	    },
	    "campaign": {
	      "id": "-KxJ3-fghKcuYo8U6Tcj",
	      "txt": "TdC Coppel Oro"
	    },
	    "provider": {
	      "uuid": "2L0Lz7YEbUePhB8GnsS58BnWlDk2",
	      "name": "Alfredo S.",
	      "avatar": "assets/imgs/avatar-provider.jpg",
	      "company": "Gruas Oriente",
	      "rating": "4.5"
	    },
	    "transaction": {
	      "redeemed": true,
	      "basePrice": 0,
	      "extraPrice": 383.4,
	      "extra": {
	        "km": 18.34,
	        "specialTow": true
	      }
	    },
	    "payment": {
	      "type": "cash",
	      "status": "paid",
	      "date": "2014-06-25T13:27:12.000Z",
	      "id": "-KxTUbyfrRYt-mW4kLt0"
	    }
  	};
  	rating=["mdi mdi-star-outline","mdi mdi-star-outline","mdi mdi-star-outline","mdi mdi-star-outline","mdi mdi-star-outline"];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	for (var i = 0; i < this.order.provider.rating; i++) {
  		this.rating[i]="mdi mdi-star";
  		this.rating[i+1]="mdi mdi-star-half";
  		console.log('entro');
  	}
  }

  ionViewDidLoad() {

  }

}
