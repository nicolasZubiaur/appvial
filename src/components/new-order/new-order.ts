import { Component } from '@angular/core';
import {NavController, ModalController} from 'ionic-angular';
import {AutocompletePage} from '../../pages/autocomplete/autocomplete';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the NewOrderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'new-order',
  templateUrl: 'new-order.html'
})
export class NewOrderComponent {
	address;
  campana={
    txt:"TDC Coppel Oro"
  };
	text: string;
  servicePicked: string;
	constructor(private navCtrl: NavController, private modalCtrl: ModalController, private alertCtrl: AlertController, private storage: Storage) {
	    this.address = {
	      place: ''
	    };
	}
  showAddressModal () {
    let modal = this.modalCtrl.create(AutocompletePage);
    modal.onDidDismiss(data => {
      this.address.place = data;
    });
    modal.present();
  }
  pickService (item){
    this.servicePicked=item;
  }

doRadio() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Cambiar Campaña');
    alert.addInput({
      type: 'radio',
      label: 'TDC Coppel Oro',
      value: 'TDC Coppel Oro'
    });

    alert.addInput({
      type: 'radio',
      label: 'Invex Credito Platino',
      value: 'Invex Credito Platino'
    });
    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Ok',
      handler: (data: any) => {
        console.log('Radio data:', data);
        if(data){
          this.campana.txt = data;
        }
        
      }
    });
    alert.present();
  }



}
