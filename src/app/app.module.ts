import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ProfilePage } from '../pages/profile/profile';
import { ServicePage } from '../pages/service/service';
import { HistoryPage } from '../pages/history/history';
import { SupportPage } from '../pages/support/support';
import { PaymentPage } from '../pages/payment/payment';
import { ReceiptPage } from '../pages/receipt/receipt';
import { SplashPage } from '../pages/splash/splash';
import { VehiclesPage } from '../pages/vehicles/vehicles';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { AutocompletePage } from '../pages/autocomplete/autocomplete';
import { FabMenuComponent } from '../components/fab-menu/fab-menu';
import { NewOrderComponent } from '../components/new-order/new-order';
import { ServiceComponent } from '../components/service/service';
import { LoaderComponent } from '../components/loader/loader';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    ProfilePage,
    ServicePage,
    HistoryPage,
    SupportPage,
    PaymentPage,
    ReceiptPage,
    SplashPage,
    FabMenuComponent,
    EditProfilePage,
    VehiclesPage,
    NewOrderComponent,
    AutocompletePage,
    ServiceComponent,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    ProfilePage,
    ServicePage,
    HistoryPage,
    SupportPage,
    PaymentPage,
    ReceiptPage,
    SplashPage,
    FabMenuComponent,
    EditProfilePage,
    VehiclesPage,
    NewOrderComponent,
    AutocompletePage,    
    ServiceComponent,
    LoaderComponent,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
