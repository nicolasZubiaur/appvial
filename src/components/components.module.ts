import { NgModule } from '@angular/core';
import { FabMenuComponent } from './fab-menu/fab-menu';
import { NewOrderComponent } from './new-order/new-order';
import { LoaderComponent } from './loader/loader';
import { ServiceComponent } from './service/service';

@NgModule({
	declarations: [FabMenuComponent,
    NewOrderComponent,
    LoaderComponent,
    ServiceComponent],
	imports: [],
	exports: [FabMenuComponent,
    NewOrderComponent,
    LoaderComponent,
    ServiceComponent]
})
export class ComponentsModule {}
