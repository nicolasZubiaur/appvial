import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FabMenuComponent } from '../../components/fab-menu/fab-menu';
import { ReceiptPage } from '../receipt/receipt';
/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
	receiptPage = ReceiptPage;
	orders=[
	{
	    "id": "-KxsN2PK_ZGJPZnN4VEJ",
	    "serviceType": {
	      "type": "tow",
	      "icon": "mdi mdi-car-hatchback",
	      "txt": "Servicio de grúa"
	    },
	    "date": "2014-01-25T12:34:56.000Z",
	   	"statustxt": "Completo",
	    "completed": true,
	    "vehicleTxt": "Jetta MKS 1287",
	    "campaignTxt": "Invex Crédito Platino ",
	    "providerName": "Fernando J."
  	},
	{
	    "id": "-KxsN2PK_ZGJPZnN4VEJ",
	    "serviceType": {
	      "type": "tow",
	      "icon": "mdi mdi-car-hatchback",
	      "txt": "Servicio de grúa"
	    },
	    "date": "2017-06-25T12:34:56.000Z",
	   	"statustxt": "Cancelado",
	    "completed": true,
	    "vehicleTxt": "Jetta MKS 1287",
	    "campaignTxt": "TdC Coppel Oro",
	    "providerName": "Rodrigo S."
  	},
	{
	    "id": "-KxsN2PK_ZGJPZnN4VEJ",
	    "serviceType": {
	      "type": "tow",
	      "icon": "mdi mdi-car-hatchback",
	      "txt": "Servicio de grúa"
	    },
	    "date": "2016-11-11T12:34:56.000Z",
	   	"statustxt": "Completo",
	    "completed": true,
	    "vehicleTxt": "Jetta MKS 1287",
	    "campaignTxt": "Invex Crédito Platino",
	    "providerName": "Juan G."
  	}

  	];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
  }

}
