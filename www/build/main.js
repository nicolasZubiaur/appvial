webpackJsonp([12],{

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutocompletePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AutocompletePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AutocompletePage = (function () {
    function AutocompletePage(viewCtrl, zone) {
        this.viewCtrl = viewCtrl;
        this.zone = zone;
        this.service = new google.maps.places.AutocompleteService();
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
    }
    AutocompletePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AutocompletePage.prototype.chooseItem = function (item) {
        this.viewCtrl.dismiss(item);
    };
    AutocompletePage.prototype.updateSearch = function () {
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var me = this;
        this.service.getPlacePredictions({ input: this.autocomplete.query, componentRestrictions: { country: 'MX' } }, function (predictions, status) {
            me.autocompleteItems = [];
            me.zone.run(function () {
                predictions.forEach(function (prediction) {
                    me.autocompleteItems.push(prediction.description);
                });
                console.log(predictions);
            });
        });
    };
    return AutocompletePage;
}());
AutocompletePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-autocomplete',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\autocomplete\autocomplete.html"*/'<ion-content padding>\n	<button block clear ion-button text-wrap text-center large margin-top>\n		<h5 margin-top>\n			<i class="mdi mdi-crosshairs-gps" padding-right></i>\n			Mi ubicación actual\n		</h5>\n	</button>\n    <ion-searchbar [animated]="true" placeholder="Buscar" [(ngModel)]="autocomplete.query" [showCancelButton]="true" (ionInput)="updateSearch()" (ionCancel)="dismiss()"></ion-searchbar>\n  <ion-list *ngIf="autocompleteItems">\n    <button ion-item text-wrap class="searchresults" *ngFor="let item of autocompleteItems" tappable (click)="chooseItem(item)">\n    	<i class="mdi mdi-map-marker" item-start></i>\n      		<h5 color="dark">{{ item }}</h5>\n    </button>\n   </ion-list>\n</ion-content>'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\autocomplete\autocomplete.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ViewController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* NgZone */]])
], AutocompletePage);

//# sourceMappingURL=autocomplete.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the EditProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EditProfilePage = (function () {
    function EditProfilePage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    EditProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditProfilePage');
    };
    EditProfilePage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    return EditProfilePage;
}());
EditProfilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-edit-profile',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\edit-profile\edit-profile.html"*/'<!--\n  Generated template for the EditProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-toolbar hideBackButton>\n    <ion-title><img src="assets/imgs/logo-text.png" class="logo-text"></ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only color="royal" (click)="closeModal()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\edit-profile\edit-profile.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ViewController */]])
], EditProfilePage);

//# sourceMappingURL=edit-profile.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReceiptPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ReceiptPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReceiptPage = (function () {
    function ReceiptPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.order = {
            "id": "-KxsN2PK_ZGJPZnN4VEJ",
            "serviceType": {
                "type": "tow",
                "icon": "mdi mdi-car-hatchback",
                "txt": "Servicio de grúa"
            },
            "date": "2014-06-25T12:34:56.000Z",
            "completed": true,
            "totalTime": "47",
            "location": {
                "lat": "19.3961817",
                "lon": "-99.2731114",
                "txt": "Fuente de Paseo 3, Lomas de las Palmas, 52788 Naucalpan de Juárez, Méx."
            },
            "status": [
                {
                    "txt": "Buscando proveedores cercanos...",
                    "timestamp": "2014-06-25T12:34:56.000Z",
                    "checked": true
                },
                {
                    "txt": "Proveedores encontrados. Asignando...",
                    "timestamp": "2014-06-25T12:44:12.000Z",
                    "checked": true
                },
                {
                    "txt": "Proveedor en camino...",
                    "timestamp": "2014-06-25T12:55:35.000Z",
                    "checked": true
                },
                {
                    "txt": "Servicio en curso",
                    "timestamp": "2014-06-25T13:04:42.000Z",
                    "checked": true
                },
                {
                    "txt": "Servicio completo",
                    "timestamp": "2014-06-25T13:27:02.000Z",
                    "checked": true
                }
            ],
            "userData": {
                "uuid": "0xv19amNS7Ys18G76Ir86jHYI1K2",
                "name": "Nicolas Z.",
                "avatar": "http://www.qygjxz.com/data/out/190/6179593-profile-pics.jpg"
            },
            "vehicle": {
                "id": "-Kq3tPRd_erDTlUOnI2d",
                "brand": "VW",
                "model": "Jetta",
                "plate": "MKS 1287"
            },
            "campaign": {
                "id": "-KxJ3-fghKcuYo8U6Tcj",
                "txt": "TdC Coppel Oro"
            },
            "provider": {
                "uuid": "2L0Lz7YEbUePhB8GnsS58BnWlDk2",
                "name": "Alfredo S.",
                "avatar": "assets/imgs/avatar-provider.jpg",
                "company": "Gruas Oriente",
                "rating": "4.5"
            },
            "transaction": {
                "redeemed": true,
                "basePrice": 0,
                "extraPrice": 383.4,
                "extra": {
                    "km": 18.34,
                    "specialTow": true
                }
            },
            "payment": {
                "type": "cash",
                "status": "paid",
                "date": "2014-06-25T13:27:12.000Z",
                "id": "-KxTUbyfrRYt-mW4kLt0"
            }
        };
        this.rating = ["mdi mdi-star-outline", "mdi mdi-star-outline", "mdi mdi-star-outline", "mdi mdi-star-outline", "mdi mdi-star-outline"];
        for (var i = 0; i < this.order.provider.rating; i++) {
            this.rating[i] = "mdi mdi-star";
            this.rating[i + 1] = "mdi mdi-star-half";
            console.log('entro');
        }
    }
    ReceiptPage.prototype.ionViewDidLoad = function () {
    };
    return ReceiptPage;
}());
ReceiptPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-receipt',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\receipt\receipt.html"*/'<ion-header>\n	<fab-menu></fab-menu>\n</ion-header>\n<ion-content padding>\n<h1 padding-left><i class="{{order.serviceType.icon}}"></i> Servicio</h1>\n<!--Proveedor-->\n<ion-card no-padding>\n  <ion-card-content no-padding>\n  	<ion-grid>\n  		<ion-row>\n  			<ion-col col-9>\n 			<h2 class="provider-name">{{order.provider.name}}</h2>\n  			<h6>\n  				<i class="rating {{item}}" *ngFor="let item of rating"></i>\n  				 {{order.provider.rating}}\n  			</h6>\n  			<h6><i class="mdi mdi-car" padding-right></i> {{order.provider.company}}</h6>\n  			\n\n  			</ion-col>\n  			<ion-col col-3>\n	  			<ion-avatar item-end>\n				  <img src="{{order.provider.avatar}}" class="provider-pic"> \n				</ion-avatar>\n  			</ion-col>\n  		</ion-row>\n  		<hr>\n  		<ion-row>\n  			<ion-col col-6>\n 			<h6><i class="mdi mdi-account-card-details" padding-right></i> {{order.campaign.txt}}</h6>\n  			<h6><i class="{{order.serviceType.icon}}" padding-right></i> {{order.serviceType.txt}}</h6>\n\n  			</ion-col>\n  			<ion-col col-6>\n          <h6><i class="mdi mdi-clock" padding-right></i> {{order.date | date:\'short\'}}</h6>\n\n  			</ion-col>  		\n  		</ion-row>\n  	</ion-grid>\n  </ion-card-content>\n</ion-card>\n\n\n<!--Timeline-->\n<h5 padding-left>Actividad del Servicio</h5>\n<ion-card>\n  <ion-card-content>\n	<ion-item text-wrap no-lines class="timeline" *ngFor="let item of order.status">\n		<i class="mdi mdi-circle completed" item-start></i>\n		<h5>\n			{{ item.txt }} \n			<br>\n			<small>{{item.timestamp | date:\'shortTime\'}}</small>\n		</h5>\n	</ion-item>\n  </ion-card-content>\n</ion-card>\n\n<!--Dirección-->\n<h5 padding-left>Detalle de servicio</h5>\n<ion-card>\n  	<ion-card-content>\n		<ion-item no-lines class="timeline" margin-top>\n  			<h6 color="primary"><i class="mdi mdi-arrow-top-right"></i><small> {{ order.location.txt  }} </small></h6>\n  			<h6 color="primary" margin-top margin-bottom><i class="mdi mdi-dots-vertical"></i> {{ order.totalTime }} minutos <small>( {{order.transaction.extra.km}}km )</small></h6>\n  			<h6 color="primary"><i class="mdi mdi-arrow-bottom-left"></i><small> {{ order.location.txt  }}</small> </h6>\n\n		</ion-item>\n	</ion-card-content>\n</ion-card>\n\n<!--Dirección-->\n<h5 padding-left>Detalle de pago</h5>\n<ion-card>\n  	<ion-card-content>\n  		<ion-item no-lines class="timeline" margin-top>\n  			<table width="100%">\n  				<tr *ngIf="order.transaction.extra.specialTow">\n  					<td>\n  						Arrastre Especial \n  					</td>\n  					<td text-right>\n  						<span>$240.00</span>\n  					</td>\n  				</tr>\n  				<tr *ngIf="order.transaction.extra.km">\n  					<td>\n  						 {{order.transaction.extra.km}} Km adicionales\n  					</td>\n  					<td text-right>\n  						<span>$77.64</span>\n  					</td>\n  				</tr>\n  				<tr>\n            <td colspan="2">\n              <hr>\n            </td>    \n          </tr> \n        \n\n  				<tr>\n  					<td>\n  						Costo Base\n  					</td>\n  					<td text-right>\n  						{{order.transaction.basePrice | currency:\'USD\':true:\'.2\'}}\n  					</td>\n  				</tr>\n  				<tr>\n  					<td>\n  						Total extras\n  					</td>\n  					<td text-right>\n  						{{order.transaction.extraPrice | currency:\'USD\':true:\'.2\'}}\n  					</td>\n  				</tr>\n  				<tr>\n  					<th text-left>\n  						Costo Total\n  					</th>\n  					<th text-right>\n  						{{order.transaction.basePrice+order.transaction.extraPrice | currency:\'USD\':true:\'.2\'}}\n  					</th>\n  				</tr>\n  				<tr>\n            <td colspan="2">\n              <hr>\n            </td>    \n          </tr> \n        \n\n  				<tr>\n  					<td>\n  						Estatus de Orden\n  					</td>\n  					<td text-right>\n  						<span class="completed" *ngIf="order.payment.status==\'paid\'">Orden Pagada</span>\n  					</td>\n  				</tr>\n  				<tr>\n  					<td>\n  						Método de Pago\n  					</td>\n  					<td text-right>\n  						<span *ngIf="order.payment.status==\'paid\'">Efectivo</span>\n  					</td>\n  				</tr>\n  				<tr>\n  					<td>\n  						Fecha de Pago\n  					</td>\n  					<td text-right>\n  						<span *ngIf="order.payment.status==\'paid\'">{{order.payment.date |date:\'short\'}}</span>\n  					</td>\n  				</tr>\n  			</table>\n\n	  	</ion-item>\n	</ion-card-content>\n\n</ion-card>\n<div padding>\n <button item-end ion-button small icon-right block color="danger">Reportar </button>\n</div>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\receipt\receipt.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], ReceiptPage);

//# sourceMappingURL=receipt.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_register__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_service__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.servicePage = __WEBPACK_IMPORTED_MODULE_3__service_service__["a" /* ServicePage */];
    }
    LoginPage.prototype.presentContactModal = function () {
        var contactModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__register_register__["a" /* RegisterPage */]);
        contactModal.present();
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\login\login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n\n<ion-content padding>\n	<div class=container3>\n		<img src="assets/imgs/logo-teleasist.png" class="logo">\n		<div class="spacer"></div>\n		<div class="inputs">\n			<ion-item>\n				<ion-label color="primary" fixed><ion-icon name="person"></ion-icon></ion-label>\n				<ion-input type="text"></ion-input>\n			</ion-item>\n			<ion-item>\n				<ion-label color="primary" fixed><ion-icon name="key"></ion-icon></ion-label>\n				<ion-input type="password"></ion-input>\n			</ion-item>\n		</div>\n		<div class="spacer"></div>\n		<button ion-button outline block color [navPush]="servicePage">Iniciar Sesión</button>\n		<button ion-button clear block color (click)=" presentContactModal()">Regístrate</button>\n	</div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\login\login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    RegisterPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    return RegisterPage;
}());
RegisterPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-register',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\register\register.html"*/'<!--\n  Generated template for the RegisterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content padding>\n\n	<div class=container3>\n\n		<img src="assets/imgs/logo-teleasist.png" class="logo2" >\n		<h3>\n			¡Con Telasist, recibe asistencia en minutos y sin problemas!<br>\n			<span class="sub">\n				Déjanos tus datos a continuación para continuar:\n			</span>\n		</h3>\n		<div class="inputs">\n\n  <ion-item>\n    <ion-label floating><ion-icon name="person"></ion-icon> Nombre</ion-label>\n    <ion-input type="text"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating><ion-icon name="mail"></ion-icon> Email</ion-label>\n    <ion-input type="text"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label floating><ion-icon name="key"></ion-icon> Contraseña</ion-label>\n    <ion-input type="password"></ion-input>\n  </ion-item>\n<ion-item text-wrap>\n  <ion-label style="font-size: 75%">Estoy de acuerdo con los Términos y Condiciones</ion-label>\n  <ion-checkbox color="primary" checked="false"></ion-checkbox>\n</ion-item>\n		</div>\n		<div class="spacer"></div>\n		<button ion-button outline block color="primary">Regístrate</button>\n		<button ion-button clear block small color="primary" (click)="dismiss()">¿Ya cuentas con usuario? Inicia Sesión</button>\n	</div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\register\register.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ViewController */]])
], RegisterPage);

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edit_profile_edit_profile__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__history_history__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__payment_payment__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__vehicles_vehicles__ = __webpack_require__(110);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(navCtrl, navParams, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.editProfilePage = __WEBPACK_IMPORTED_MODULE_2__edit_profile_edit_profile__["a" /* EditProfilePage */];
        this.historyPage = __WEBPACK_IMPORTED_MODULE_3__history_history__["a" /* HistoryPage */];
        this.paymentPage = __WEBPACK_IMPORTED_MODULE_4__payment_payment__["a" /* PaymentPage */];
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.editProfileModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__edit_profile_edit_profile__["a" /* EditProfilePage */]);
        modal.present();
    };
    ProfilePage.prototype.vehiclesModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__vehicles_vehicles__["a" /* VehiclesPage */]);
        modal.present();
    };
    return ProfilePage;
}());
ProfilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-profile',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\profile\profile.html"*/'<ion-header>\n  <fab-menu></fab-menu>\n</ion-header>\n<ion-content padding>\n<ion-grid>\n	<ion-row>\n		<ion-col col-3 text-right>\n			<div class="button-cont-mdi">\n				<i class="mdi mdi-camera button-mdi"></i>\n			</div>\n		</ion-col>\n		<ion-col col-6 text-center>\n			<img class="avatar" style="background-image:url(\'assets/imgs/avatar.png\');" />\n		</ion-col>\n		<ion-col col-3 text-left>\n			<div class="button-cont-mdi">\n				<i class="mdi mdi-pencil button-mdi"></i>\n			</div>\n		</ion-col>\n	</ion-row>\n</ion-grid>\n<h2 class="profilename" text-center> Nicolas Zubiaur</h2>\n<ion-card margin-top>\n  <ion-list>\n    <button ion-item (click)="editProfileModal()">\n      <ion-icon name="information-circle" item-start></ion-icon>\n      Editar Datos\n    </button>\n\n    <button ion-item (click)="vehiclesModal()">\n      <ion-icon name="car" item-start></ion-icon>\n      Mis Vehículos\n    </button>\n\n    <button ion-item [navPush]="paymentPage">\n      <ion-icon name="card" item-start></ion-icon>\n      Métodos de Pago\n    </button>\n\n    <button ion-item [navPush]="historyPage">\n      <ion-icon name="clock" item-start></ion-icon>\n      Historial\n    </button>\n\n    <button ion-item>\n      <ion-icon name="share" item-start></ion-icon>\n      Compartir\n    </button>\n\n  </ion-list>\n</ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\profile\profile.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
], ProfilePage);

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VehiclesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the VehiclesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VehiclesPage = (function () {
    function VehiclesPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    VehiclesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad VehiclesPage');
    };
    VehiclesPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    return VehiclesPage;
}());
VehiclesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-vehicles',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\vehicles\vehicles.html"*/'<!--\n  Generated template for the VehiclesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-toolbar hideBackButton>\n    <ion-title><img src="assets/imgs/logo-text.png" class="logo-text"></ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only color="royal" (click)="closeModal()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\vehicles\vehicles.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ViewController */]])
], VehiclesPage);

//# sourceMappingURL=vehicles.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplashPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SplashPage = (function () {
    function SplashPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loginPage = __WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */];
    }
    SplashPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SplashPage');
    };
    return SplashPage;
}());
SplashPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-splash',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\splash\splash.html"*/'<!--\n  Generated template for the SplashPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content padding>\n    <ion-card class="container slide-in-both-ways">\n      <img src="assets/imgs/head-1.jpg">\n      <ion-card-header text-wrap>\n        <strong>Tel</strong>Asist \n        App<strong>Vial</strong>\n      </ion-card-header>\n    \n      <ion-card-content>\n        <img src="assets/imgs/logo-teleasist-black.png" class="logo">\n        <h2 color="light">¡Recibe asitencia en el camino en minutos!</h2>\n        <button ion-button block clear icon-right [navPush]="loginPage">\n          Comenzar\n          <ion-icon name="send"></ion-icon>\n        </button>\n      </ion-card-content>\n    \n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\splash\splash.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], SplashPage);

//# sourceMappingURL=splash.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SupportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SupportPage = (function () {
    function SupportPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SupportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SupportPage');
    };
    return SupportPage;
}());
SupportPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-support',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\support\support.html"*/'<ion-header>\n	<fab-menu></fab-menu>\n</ion-header>\n<ion-content padding>\n<h1>Support</h1>\n</ion-content>'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\support\support.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], SupportPage);

//# sourceMappingURL=support.js.map

/***/ }),

/***/ 120:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 120;

/***/ }),

/***/ 161:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/autocomplete/autocomplete.module": [
		283,
		11
	],
	"../pages/edit-profile/edit-profile.module": [
		284,
		10
	],
	"../pages/history/history.module": [
		285,
		9
	],
	"../pages/login/login.module": [
		286,
		8
	],
	"../pages/payment/payment.module": [
		287,
		7
	],
	"../pages/profile/profile.module": [
		288,
		6
	],
	"../pages/receipt/receipt.module": [
		290,
		5
	],
	"../pages/register/register.module": [
		289,
		4
	],
	"../pages/service/service.module": [
		291,
		3
	],
	"../pages/splash/splash.module": [
		292,
		2
	],
	"../pages/support/support.module": [
		293,
		1
	],
	"../pages/vehicles/vehicles.module": [
		294,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 161;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_web_animations_js_web_animations_min__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_web_animations_js_web_animations_min___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_web_animations_js_web_animations_min__);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);

//# sourceMappingURL=main.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_register_register__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_profile_profile__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_service_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_history_history__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_support_support__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_payment_payment__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_receipt_receipt__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_splash_splash__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_vehicles_vehicles__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_edit_profile_edit_profile__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_autocomplete_autocomplete__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_fab_menu_fab_menu__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_new_order_new_order__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_service_service__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_loader_loader__ = __webpack_require__(281);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_register_register__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_service_service__["a" /* ServicePage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_history_history__["a" /* HistoryPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_support_support__["a" /* SupportPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_payment_payment__["a" /* PaymentPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_receipt_receipt__["a" /* ReceiptPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_splash_splash__["a" /* SplashPage */],
            __WEBPACK_IMPORTED_MODULE_20__components_fab_menu_fab_menu__["a" /* FabMenuComponent */],
            __WEBPACK_IMPORTED_MODULE_18__pages_edit_profile_edit_profile__["a" /* EditProfilePage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_vehicles_vehicles__["a" /* VehiclesPage */],
            __WEBPACK_IMPORTED_MODULE_21__components_new_order_new_order__["a" /* NewOrderComponent */],
            __WEBPACK_IMPORTED_MODULE_19__pages_autocomplete_autocomplete__["a" /* AutocompletePage */],
            __WEBPACK_IMPORTED_MODULE_22__components_service_service__["a" /* ServiceComponent */],
            __WEBPACK_IMPORTED_MODULE_23__components_loader_loader__["a" /* LoaderComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/autocomplete/autocomplete.module#AutocompletePageModule', name: 'AutocompletePage', segment: 'autocomplete', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/edit-profile/edit-profile.module#EditProfilePageModule', name: 'EditProfilePage', segment: 'edit-profile', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/history/history.module#HistoryPageModule', name: 'HistoryPage', segment: 'history', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/payment/payment.module#PaymentPageModule', name: 'PaymentPage', segment: 'payment', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/receipt/receipt.module#ReceiptPageModule', name: 'ReceiptPage', segment: 'receipt', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/service/service.module#ServicePageModule', name: 'ServicePage', segment: 'service', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/splash/splash.module#SplashPageModule', name: 'SplashPage', segment: 'splash', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/support/support.module#SupportPageModule', name: 'SupportPage', segment: 'support', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/vehicles/vehicles.module#VehiclesPageModule', name: 'VehiclesPage', segment: 'vehicles', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["a" /* IonicStorageModule */].forRoot()
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_register_register__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_service_service__["a" /* ServicePage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_history_history__["a" /* HistoryPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_support_support__["a" /* SupportPage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_payment_payment__["a" /* PaymentPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_receipt_receipt__["a" /* ReceiptPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_splash_splash__["a" /* SplashPage */],
            __WEBPACK_IMPORTED_MODULE_20__components_fab_menu_fab_menu__["a" /* FabMenuComponent */],
            __WEBPACK_IMPORTED_MODULE_18__pages_edit_profile_edit_profile__["a" /* EditProfilePage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_vehicles_vehicles__["a" /* VehiclesPage */],
            __WEBPACK_IMPORTED_MODULE_21__components_new_order_new_order__["a" /* NewOrderComponent */],
            __WEBPACK_IMPORTED_MODULE_19__pages_autocomplete_autocomplete__["a" /* AutocompletePage */],
            __WEBPACK_IMPORTED_MODULE_22__components_service_service__["a" /* ServiceComponent */],
            __WEBPACK_IMPORTED_MODULE_23__components_loader_loader__["a" /* LoaderComponent */],
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_splash_splash__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_splash_splash__["a" /* SplashPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\home\home.html"*/'<ion-header>\n	<fab-menu></fab-menu>\n</ion-header>\n<ion-content padding>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\home\home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FabMenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_profile_profile__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_history_history__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_payment_payment__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_support_support__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_service_service__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the FabMenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var FabMenuComponent = (function () {
    function FabMenuComponent(navCtrl, events) {
        this.navCtrl = navCtrl;
        this.events = events;
        this.profilePage = __WEBPACK_IMPORTED_MODULE_2__pages_profile_profile__["a" /* ProfilePage */];
        this.historyPage = __WEBPACK_IMPORTED_MODULE_3__pages_history_history__["a" /* HistoryPage */];
        this.paymentPage = __WEBPACK_IMPORTED_MODULE_4__pages_payment_payment__["a" /* PaymentPage */];
        this.servicePage = __WEBPACK_IMPORTED_MODULE_6__pages_service_service__["a" /* ServicePage */];
        this.supportPage = __WEBPACK_IMPORTED_MODULE_5__pages_support_support__["a" /* SupportPage */];
        this.profileTxt = 'ProfilePage';
        this.historyTxt = 'HistoryPage';
        this.paymentTxt = 'PaymentPage';
        this.serviceTxt = 'ServicePage';
        this.supportTxt = 'SupportPage';
    }
    FabMenuComponent.prototype.ngAfterViewInit = function () {
        switch (this.navCtrl.getActive().name) {
            case 'ProfilePage':
                this.activePage = 1;
                break;
            case 'HistoryPage':
                this.activePage = 2;
                break;
            case 'PaymentPage':
                this.activePage = 3;
                break;
            case 'ServicePage':
                this.activePage = 4;
                break;
            case 'SupportPage':
                this.activePage = 5;
                break;
            default:
                this.activePage = null;
                break;
        }
    };
    return FabMenuComponent;
}());
FabMenuComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'fab-menu',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\components\fab-menu\fab-menu.html"*/'<!-- Generated template for the FabMenuComponent component -->\n  <ion-navbar>\n    <ion-title><img src="assets/imgs/logo-text.png" style="max-height: 85%; height: 30px"></ion-title>\n  </ion-navbar>\n	<ion-fab bottom right edge>\n	    <button ion-fab color="darkblue">\n	    	<i class="mdi mdi-menu"></i>\n	    </button>\n	    <ion-fab-list side="bottom">\n	      <button ion-fab [disabled]="activePage==1" [color]="(activePage==1 ? \'darkblue\' : \'light\')" [navPush]="profilePage">\n	      	<i class="mdi mdi-account"></i>\n	      </button>\n	      <small>Perfil</small>\n\n	      <button ion-fab [disabled]="activePage==4" [color]="(activePage==4 ? \'darkblue\' : \'light\')" [navPush]="servicePage">\n	      	<i class="mdi mdi-car"></i>\n	      </button>\n	      <small>Servicio</small>\n\n	      <button ion-fab [disabled]="activePage==2" [color]="(activePage==2 ? \'darkblue\' : \'light\')" [navPush]="historyPage">\n	      	<i class="mdi mdi-clock"></i>\n	      </button>\n	      <small>Historial</small>\n\n	      <button ion-fab [disabled]="activePage==3" [color]="(activePage==3 ? \'darkblue\' : \'light\')" [navPush]="paymentPage">\n	      	<i class="mdi mdi-credit-card"></i>\n	      </button>\n	      <small text-center>Pagos y Campañas</small>\n\n	      <button ion-fab [disabled]="activePage==5" [color]="(activePage==5 ? \'darkblue\' : \'light\')" [navPush]="supportPage">\n	      	<i class="mdi mdi-help"></i>\n	      </button>\n	      <small>Ayuda</small>\n\n	    </ion-fab-list>\n	</ion-fab>\n'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\components\fab-menu\fab-menu.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]])
], FabMenuComponent);

//# sourceMappingURL=fab-menu.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewOrderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_autocomplete_autocomplete__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the NewOrderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var NewOrderComponent = (function () {
    function NewOrderComponent(navCtrl, modalCtrl, alertCtrl, storage) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.campana = {
            txt: "TDC Coppel Oro"
        };
        this.address = {
            place: ''
        };
    }
    NewOrderComponent.prototype.showAddressModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__pages_autocomplete_autocomplete__["a" /* AutocompletePage */]);
        modal.onDidDismiss(function (data) {
            _this.address.place = data;
        });
        modal.present();
    };
    NewOrderComponent.prototype.pickService = function (item) {
        this.servicePicked = item;
    };
    NewOrderComponent.prototype.doRadio = function () {
        var _this = this;
        var alert = this.alertCtrl.create();
        alert.setTitle('Cambiar Campaña');
        alert.addInput({
            type: 'radio',
            label: 'TDC Coppel Oro',
            value: 'TDC Coppel Oro'
        });
        alert.addInput({
            type: 'radio',
            label: 'Invex Credito Platino',
            value: 'Invex Credito Platino'
        });
        alert.addButton('Cancelar');
        alert.addButton({
            text: 'Ok',
            handler: function (data) {
                console.log('Radio data:', data);
                if (data) {
                    _this.campana.txt = data;
                }
            }
        });
        alert.present();
    };
    return NewOrderComponent;
}());
NewOrderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'new-order',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\components\new-order\new-order.html"*/'<h1 padding-left><i class="mdi mdi-plus"></i> Nuevo Servicio</h1>  <h4>\n\n<!--Cuenta-->\n<h5 padding-left>Campaña</h5>\n  <ion-item text-wrap (click)="doRadio() ">\n    <ion-avatar item-start>\n      <img src="assets/imgs/avatar.png"> \n    </ion-avatar>\n    <h3> {{campana.txt}}</h3>\n    <i class="mdi mdi-share" item-end></i>\n  </ion-item>\n\n<!--Tipo de Servicio-->\n<h5 padding-left>Tipo de Servicio</h5>\n\n\n<ion-list no-lines>\n  <ion-item>\n \n	<ion-grid class="button-grid">\n		<ion-row>\n			<ion-col col-2 text-center [ngClass]="servicePicked === \'battery\' ? \'primarytxt\' : \'darktxt\'" (click)="pickService(\'battery\')">\n				<i class="mdi mdi-car-battery"></i>\n				<br>\n				<small>(2/4)</small>\n			</ion-col>\n			<ion-col col-2 text-center [ngClass]="servicePicked === \'tow\' ? \'primarytxt\' : \'darktxt\'" (click)="pickService(\'tow\')">\n				<i class="mdi mdi-car-hatchback"></i>\n				<br>\n				<small>(2/4)</small>\n			</ion-col>\n			<ion-col col-2 text-center [ngClass]="servicePicked === \'fix\' ? \'primarytxt\' : \'darktxt\'" (click)="pickService(\'fix\')">\n				<i class="mdi mdi-wrench"></i>\n				<br>\n				<small>(2/4)</small>\n			</ion-col>\n      <ion-col col-2 text-center  [ngClass]="servicePicked === \'key\' ? \'primarytxt\' : \'darktxt\'" (click)="pickService(\'key\')">\n        <i class="mdi mdi-key-variant"></i>\n        <br>\n        <small>(2/4)</small>\n      </ion-col>\n      <ion-col col-2 text-center  [ngClass]="servicePicked === \'gas\' ? \'primarytxt\' : \'darktxt\'" (click)="pickService(\'gas\')">\n        <i class="mdi mdi-gas-station"></i>\n        <br>\n        <small>(2/4)</small>\n      </ion-col>\n      <ion-col col-2 text-center [ngClass]="servicePicked === \'wash\' ? \'primarytxt\' : \'darktxt\'" (click)="pickService(\'wash\')">\n        <i class="mdi mdi-car-wash"></i>\n        <br>\n        <small>(2/4)</small>\n      </ion-col>\n		</ion-row>\n	</ion-grid>\n \n  </ion-item>\n\n\n<!--Dirección de Servicio-->\n\n\n    \n    <ion-item text-wrap (click)="showAddressModal()" no-lines>\n   		<ion-icon name="pin" item-start></ion-icon>\n   		<p *ngIf="!address.place!==\'\'" class="label-input">¿Dónde se encuentra el vehículo?</p>\n      	<p text-right *ngIf="address.place!==\'\'">{{address.place}}</p>\n    </ion-item>\n  </ion-list>\n\n\n<!-- Vehículo -->\n<h5 padding-left>Datos de la asistencia</h5>\n<ion-list>\n  <ion-item>\n    <ion-label><i class="mdi mdi-car-hatchback"></i> <small padding-left>Vehículo</small></ion-label>\n    <ion-select [(ngModel)]="gaming"  interface="popover">\n      <ion-option value="1">Jetta MKV3386</ion-option>\n      <ion-option value="2">Armada NSA1232</ion-option>\n      <ion-option value="new">+ Nuevo Vehículo</ion-option>\n    </ion-select>\n  </ion-item>\n\n<!-- Preguntas y Extras -->\n\n  <ion-item><span item-end>{{crash ? \'Si\':\'No\'}}</span>\n    <ion-label><small>¿El vehículo sufrió un accidente? </small></ion-label>\n    <ion-toggle [(ngModel)]="crash"></ion-toggle>\n  </ion-item>\n  <ion-item>\n    <ion-label><small>¿Se requiere equipo especial? <span item-end>{{special ? \'Si\':\'No\'}}</span></small></ion-label>\n    <ion-toggle [(ngModel)]="special"></ion-toggle>\n  </ion-item>\n</ion-list>\n\n\n'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\components\new-order\new-order.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
], NewOrderComponent);

//# sourceMappingURL=new-order.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the ServiceComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ServiceComponent = (function () {
    function ServiceComponent() {
        var _this = this;
        this.order = {
            "id": "-KxsN2PK_ZGJPZnN4VEJ",
            "serviceType": {
                "type": "tow",
                "icon": "mdi mdi-car-hatchback",
                "txt": "Servicio de grúa"
            },
            "date": "2014-06-25T12:34:56.000Z",
            "completed": true,
            "totalTime": "47",
            "location": {
                "lat": "19.3961817",
                "lon": "-99.2731114",
                "txt": "Fuente de Paseo 3, Lomas de las Palmas, 52788 Naucalpan de Juárez, Méx."
            },
            "status": [
                {
                    "txt": "Buscando proveedores cercanos...",
                    "timestamp": null,
                    "checked": false
                },
                {
                    "txt": "Proveedores encontrados. Asignando...",
                    "timestamp": null,
                    "checked": false
                },
                {
                    "txt": "Proveedor en camino...",
                    "timestamp": null,
                    "checked": false
                },
                {
                    "txt": "Servicio en curso",
                    "timestamp": null,
                    "checked": false
                },
                {
                    "txt": "Servicio completo",
                    "timestamp": null,
                    "checked": false
                }
            ],
            "userData": {
                "uuid": "0xv19amNS7Ys18G76Ir86jHYI1K2",
                "name": "Nicolas Z.",
                "avatar": "http://www.qygjxz.com/data/out/190/6179593-profile-pics.jpg"
            },
            "vehicle": {
                "id": "-Kq3tPRd_erDTlUOnI2d",
                "brand": "VW",
                "model": "Jetta",
                "plate": "MKS 1287"
            },
            "campaign": {
                "id": "-KxJ3-fghKcuYo8U6Tcj",
                "txt": "TdC Coppel Oro"
            },
            "provider": {
                "uuid": "2L0Lz7YEbUePhB8GnsS58BnWlDk2",
                "name": "Alfredo S.",
                "avatar": "assets/imgs/avatar-provider.jpg",
                "company": "Gruas Oriente",
                "rating": "4.5"
            },
            "transaction": {
                "redeemed": true,
                "basePrice": 0,
                "extraPrice": 383.4,
                "extra": {
                    "km": 18.34,
                    "specialTow": true
                }
            },
            "payment": {
                "type": "cash",
                "status": "paid",
                "date": "2014-06-25T13:27:12.000Z",
                "id": "-KxTUbyfrRYt-mW4kLt0"
            }
        };
        setTimeout(function () {
            _this.order.status[0].timestamp = new Date();
            _this.order.status[0].checked = true;
        }, 5000);
        setTimeout(function () {
            _this.order.status[1].timestamp = new Date();
            _this.order.status[1].checked = true;
        }, 10000);
        setTimeout(function () {
            _this.order.status[2].timestamp = new Date();
            _this.order.status[2].checked = true;
        }, 15000);
        setTimeout(function () {
            _this.order.status[3].timestamp = new Date();
            _this.order.status[3].checked = true;
        }, 20000);
        setTimeout(function () {
            _this.order.status[4].timestamp = new Date();
            _this.order.status[4].checked = true;
        }, 25000);
    }
    return ServiceComponent;
}());
ServiceComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'service',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\components\service\service.html"*/'<h1 padding-left><i class="{{order.serviceType.icon}}"></i> Servicio en Curso</h1>\n<!--Proveedor-->\n\n  <ion-item>\n  	<ion-grid>\n  		<ion-row>\n  			<ion-col col-9>\n 			<h2 class="provider-name">{{order.provider.name}}</h2>\n  			<h6>\n  				<i class="rating {{item}}" *ngFor="let item of rating"></i>\n  				 {{order.provider.rating}}\n  			</h6>\n  			<h6><i class="mdi mdi-car" padding-right></i> {{order.provider.company}}</h6>\n  			\n\n  			</ion-col>\n  			<ion-col col-3>\n	  			<ion-avatar item-end>\n				  <img src="{{order.provider.avatar}}" class="provider-pic"> \n				</ion-avatar>\n  			</ion-col>\n  		</ion-row>\n  		<hr>\n  		<ion-row>\n  			<ion-col col-6>\n 			<h6><i class="mdi mdi-account-card-details" padding-right></i> {{order.campaign.txt}}</h6>\n  			<h6><i class="{{order.serviceType.icon}}" padding-right></i> {{order.serviceType.txt}}</h6>\n\n  			</ion-col>\n  			<ion-col col-6>\n          <h6><i class="mdi mdi-clock" padding-right></i> {{order.date | date:\'short\'}}</h6>\n\n  			</ion-col>  		\n  		</ion-row>\n  	</ion-grid>\n  </ion-item>\n\n\n\n<!--Timeline-->\n<h5 padding-left>Actividad del Servicio</h5>\n\n	<ion-item text-wrap no-lines class="timeline" *ngFor="let item of order.status">\n		<i class="mdi mdi-circle" [ngClass]="item.checked ? \'secondarytxt\' : \'stabletxt\'" item-start></i>\n		<h5  *ngIf="item.checked">\n			<small><em>{{ item.txt }}</em> </small>\n			<br>\n			<small>{{item.timestamp | date:\'shortTime\'}}</small>\n		</h5>\n    <h5  *ngIf="!item.checked">\n      {{ item.txt }}\n      <br>\n      <small>{{item.timestamp | date:\'shortTime\'}}</small>\n    </h5>\n	</ion-item>\n\n\n<!--Dirección-->\n<h5 padding-left>Detalle de pago</h5>\n\n  		<ion-item no-lines class="timeline" padding-top padding-bottom>\n  			<table width="100%">\n  				<tr *ngIf="order.transaction.extra.specialTow">\n  					<td>\n  						Arrastre Especial \n  					</td>\n  					<td text-right>\n  						<span>$240.00</span>\n  					</td>\n  				</tr>\n  				<tr *ngIf="order.transaction.extra.km">\n  					<td>\n  						 {{order.transaction.extra.km}} Km adicionales\n  					</td>\n  					<td text-right>\n  						<span>$77.64</span>\n  					</td>\n  				</tr>\n  				<tr>\n            <td colspan="2">\n              <hr>\n            </td>    \n          </tr> \n        \n\n  				<tr>\n  					<td>\n  						Costo Base\n  					</td>\n  					<td text-right>\n  						{{order.transaction.basePrice | currency:\'USD\':true:\'.2\'}}\n  					</td>\n  				</tr>\n  				<tr>\n  					<td>\n  						Total extras\n  					</td>\n  					<td text-right>\n  						{{order.transaction.extraPrice | currency:\'USD\':true:\'.2\'}}\n  					</td>\n  				</tr>\n  				<tr>\n  					<th text-left>\n  						Costo Total\n  					</th>\n  					<th text-right>\n  						{{order.transaction.basePrice+order.transaction.extraPrice | currency:\'USD\':true:\'.2\'}}\n  					</th>\n  				</tr>\n  				<tr>\n            <td colspan="2">\n              <hr>\n            </td>    \n          </tr> \n        \n\n  				<tr>\n  					<td>\n  						Estatus de Orden\n  					</td>\n  					<td text-right>\n  						<span class="completed" *ngIf="order.payment.status==\'paid\'">Orden Pagada</span>\n  					</td>\n  				</tr>\n  				<tr>\n  					<td>\n  						Método de Pago\n  					</td>\n  					<td text-right>\n  						<span *ngIf="order.payment.status==\'paid\'">Efectivo</span>\n  					</td>\n  				</tr>\n  				<tr>\n  					<td>\n  						Fecha de Pago\n  					</td>\n  					<td text-right>\n  						<span *ngIf="order.payment.status==\'paid\'">{{order.payment.date |date:\'short\'}}</span>\n  					</td>\n  				</tr>\n  			</table>\n\n	  	</ion-item>\n\n<div padding>\n <button item-end ion-button small icon-right block color="danger">Reportar </button>\n</div>'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\components\service\service.html"*/
    }),
    __metadata("design:paramtypes", [])
], ServiceComponent);

//# sourceMappingURL=service.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the LoaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var LoaderComponent = (function () {
    function LoaderComponent() {
        console.log('Hello LoaderComponent Component');
        this.text = 'Hello World';
    }
    return LoaderComponent;
}());
LoaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'loader',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\components\loader\loader.html"*/'<!-- Generated template for the LoaderComponent component -->\n<div>\n	<div class=container4>\n  <p text-center >\n  	<img src="assets/imgs/message.svg" style="width: 60%"> <br>\n  	Estamos buscando al proveedor mas cercano. <br>Este proceso puede tomar tiempo, <br>por favor espera con nosotros.\n  </p>\n</div>\n\n</div>\n'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\components\loader\loader.html"*/
    }),
    __metadata("design:paramtypes", [])
], LoaderComponent);

//# sourceMappingURL=loader.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__receipt_receipt__ = __webpack_require__(106);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HistoryPage = (function () {
    function HistoryPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.receiptPage = __WEBPACK_IMPORTED_MODULE_2__receipt_receipt__["a" /* ReceiptPage */];
        this.orders = [
            {
                "id": "-KxsN2PK_ZGJPZnN4VEJ",
                "serviceType": {
                    "type": "tow",
                    "icon": "mdi mdi-car-hatchback",
                    "txt": "Servicio de grúa"
                },
                "date": "2014-01-25T12:34:56.000Z",
                "statustxt": "Completo",
                "completed": true,
                "vehicleTxt": "Jetta MKS 1287",
                "campaignTxt": "Invex Crédito Platino ",
                "providerName": "Fernando J."
            },
            {
                "id": "-KxsN2PK_ZGJPZnN4VEJ",
                "serviceType": {
                    "type": "tow",
                    "icon": "mdi mdi-car-hatchback",
                    "txt": "Servicio de grúa"
                },
                "date": "2017-06-25T12:34:56.000Z",
                "statustxt": "Cancelado",
                "completed": true,
                "vehicleTxt": "Jetta MKS 1287",
                "campaignTxt": "TdC Coppel Oro",
                "providerName": "Rodrigo S."
            },
            {
                "id": "-KxsN2PK_ZGJPZnN4VEJ",
                "serviceType": {
                    "type": "tow",
                    "icon": "mdi mdi-car-hatchback",
                    "txt": "Servicio de grúa"
                },
                "date": "2016-11-11T12:34:56.000Z",
                "statustxt": "Completo",
                "completed": true,
                "vehicleTxt": "Jetta MKS 1287",
                "campaignTxt": "Invex Crédito Platino",
                "providerName": "Juan G."
            }
        ];
    }
    HistoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HistoryPage');
    };
    return HistoryPage;
}());
HistoryPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-history',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\history\history.html"*/'<ion-header>\n	<fab-menu></fab-menu>\n</ion-header>\n<ion-content padding>\n	<h1 padding-left><i class="mdi mdi-clock"></i> Historial</h1>\n<ion-list>\n  <ion-item *ngFor="let order of orders" [navPush]="receiptPage" text-wrap>\n\n    <h6 item-end>\n		<small>\n			<i class="mdi mdi-clock" padding-right></i>\n			{{ order.date | date:\'short\' }}\n			<br>\n			<strong>\n				<i class="mdi mdi-car-hatchback" padding-right></i>\n				{{ order.vehicleTxt }}\n			</strong>\n			<br>\n  			<span *ngIf="order.statustxt==\'Cancelado\'" class="dangertxt">Orden Cancelada</span>\n  			<span *ngIf="order.statustxt==\'Completo\'" class="secondarytxt">Orden Completa</span>\n		</small>\n	</h6>\n  	<h6>\n  			<strong>{{ order.serviceType.txt }}</strong>\n  			<br>\n  			{{ order.providerName }}\n\n  	</h6>\n    <i class="mdi mdi-arrow-right" item-end></i>\n  </ion-item>  \n</ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\history\history.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], HistoryPage);

//# sourceMappingURL=history.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ServicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ServicePage = (function () {
    function ServicePage(navCtrl, navParams, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.serviceStatus = 'new';
    }
    ServicePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ServicePage');
    };
    ServicePage.prototype.sendOrder = function () {
        var _this = this;
        this.serviceStatus = "loading";
        setTimeout(function () {
            _this.serviceStatus = "service";
        }, 3000);
    };
    return ServicePage;
}());
ServicePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-service',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\service\service.html"*/'<ion-header>\n	<fab-menu></fab-menu>\n</ion-header>\n<ion-content padding>\n<div *ngIf="serviceStatus==\'new\'">\n	<new-order></new-order>\n	<div padding-right padding-left margin-top>\n	   <button ion-button block icon-right color="darkblue" (click)="sendOrder()">\n	      Siguiente\n	  </button>\n	</div>\n</div>	\n<loader *ngIf="serviceStatus==\'loading\'"></loader>\n<service *ngIf="serviceStatus==\'service\'"></service>\n</ion-content>'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\service\service.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], ServicePage);

//# sourceMappingURL=service.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PaymentPage = (function () {
    function PaymentPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PaymentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PaymentPage');
    };
    return PaymentPage;
}());
PaymentPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-payment',template:/*ion-inline-start:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\payment\payment.html"*/'<ion-header>\n	<fab-menu></fab-menu>\n</ion-header>\n<ion-content padding>\n<h1>Payment</h1>\n</ion-content>'/*ion-inline-end:"C:\Users\Zubiaur\Desktop\ngProjects\appvial\src\pages\payment\payment.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], PaymentPage);

//# sourceMappingURL=payment.js.map

/***/ })

},[205]);
//# sourceMappingURL=main.js.map