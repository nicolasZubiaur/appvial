import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { ProfilePage } from '../../pages/profile/profile';
import { HistoryPage } from '../../pages/history/history';
import { PaymentPage } from '../../pages/payment/payment';
import { SupportPage } from '../../pages/support/support';
import { ServicePage } from '../../pages/service/service';
/**
 * Generated class for the FabMenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'fab-menu',
  templateUrl: 'fab-menu.html'
})
export class FabMenuComponent {
	profilePage = ProfilePage;
	historyPage = HistoryPage;
	paymentPage = PaymentPage;
	servicePage = ServicePage;
	supportPage = SupportPage;
	profileTxt='ProfilePage';
	historyTxt = 'HistoryPage';
	paymentTxt = 'PaymentPage';
	serviceTxt = 'ServicePage';
	supportTxt = 'SupportPage';
	activePage;
	constructor(public navCtrl: NavController, public events: Events) {

	}
	ngAfterViewInit() {
	 switch (	this.navCtrl.getActive().name) {
	 	case 'ProfilePage':
	 			this.activePage=1;
	 		break;
	 	case 'HistoryPage':
	 			this.activePage=2;
	 		break;
	 	case 'PaymentPage':
	 			this.activePage=3;
	 		break;
	 	case 'ServicePage':
	 			this.activePage=4;
	 		break;
	 	case 'SupportPage':
	 			this.activePage=5;
	 		break;
	 	default:
	 		this.activePage=null;
	 		break;
	 }
	}
    
}
