import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FabMenuComponent } from '../../components/fab-menu/fab-menu';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

}
