import { Component } from '@angular/core';

/**
 * Generated class for the ServiceComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'service',
  templateUrl: 'service.html'
})
export class ServiceComponent {
	order=
	{
	    "id": "-KxsN2PK_ZGJPZnN4VEJ",
	    "serviceType": {
	      "type": "tow",
	      "icon": "mdi mdi-car-hatchback",
	      "txt": "Servicio de grúa"
	    },
	    "date": "2014-06-25T12:34:56.000Z",
	    "completed": true,
	    "totalTime": "47",
	    "location": {
	      "lat": "19.3961817",
	      "lon": "-99.2731114",
	      "txt": "Fuente de Paseo 3, Lomas de las Palmas, 52788 Naucalpan de Juárez, Méx."
	    },
	    "status": [
	      {
	        "txt": "Buscando proveedores cercanos...",
	        "timestamp": null,
	        "checked": false
	      },
	      {
	        "txt": "Proveedores encontrados. Asignando...",
	        "timestamp": null,
	        "checked": false
	      },
	      {
	        "txt": "Proveedor en camino...",
	        "timestamp": null,
	        "checked": false
	      },
	      {
	        "txt": "Servicio en curso",
	        "timestamp": null,
	        "checked": false
	      },
	      {
	        "txt": "Servicio completo",
	        "timestamp": null,
	        "checked": false
	      }
	    ],
	    "userData": {
	      "uuid": "0xv19amNS7Ys18G76Ir86jHYI1K2",
	      "name": "Nicolas Z.",
	      "avatar": "http://www.qygjxz.com/data/out/190/6179593-profile-pics.jpg"
	    },
	    "vehicle": {
	      "id": "-Kq3tPRd_erDTlUOnI2d",
	      "brand": "VW",
	      "model": "Jetta",
	      "plate": "MKS 1287"
	    },
	    "campaign": {
	      "id": "-KxJ3-fghKcuYo8U6Tcj",
	      "txt": "TdC Coppel Oro"
	    },
	    "provider": {
	      "uuid": "2L0Lz7YEbUePhB8GnsS58BnWlDk2",
	      "name": "Alfredo S.",
	      "avatar": "assets/imgs/avatar-provider.jpg",
	      "company": "Gruas Oriente",
	      "rating": "4.5"
	    },
	    "transaction": {
	      "redeemed": true,
	      "basePrice": 0,
	      "extraPrice": 383.4,
	      "extra": {
	        "km": 18.34,
	        "specialTow": true
	      }
	    },
	    "payment": {
	      "type": "cash",
	      "status": "paid",
	      "date": "2014-06-25T13:27:12.000Z",
	      "id": "-KxTUbyfrRYt-mW4kLt0"
	    }
  	};
  text: string;

  constructor() {

	setTimeout( () => {
		this.order.status[0].timestamp =    new Date();
		this.order.status[0].checked =    true;
	}, 5000);
	setTimeout( () => {
		this.order.status[1].timestamp =    new Date();
		this.order.status[1].checked =    true;
	}, 10000);
	setTimeout( () => {
		this.order.status[2].timestamp =    new Date();
		this.order.status[2].checked =    true;
	}, 15000);
	setTimeout( () => {
		this.order.status[3].timestamp =    new Date();
		this.order.status[3].checked =    true;
	}, 20000);
	setTimeout( () => {
		this.order.status[4].timestamp =    new Date();
		this.order.status[4].checked =    true;
	}, 25000);
  }

}
