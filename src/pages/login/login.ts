import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { ServicePage } from '../service/service';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
	servicePage=ServicePage;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }
 presentContactModal() {
   let contactModal = this.modalCtrl.create(RegisterPage);
   contactModal.present();
 }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
