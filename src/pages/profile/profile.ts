import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FabMenuComponent } from '../../components/fab-menu/fab-menu';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { HistoryPage } from '../history/history';
import { PaymentPage } from '../payment/payment';
import { VehiclesPage } from '../vehicles/vehicles';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
	editProfilePage = EditProfilePage;
	historyPage = HistoryPage;
	paymentPage = PaymentPage;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  editProfileModal() {
    let modal = this.modalCtrl.create(EditProfilePage);
    modal.present();
  }
  vehiclesModal() {
    let modal = this.modalCtrl.create(VehiclesPage);
    modal.present();
  }
}
